package avance.proyecto.bl;

public class Administrador {


    private  String nombre;
    private String primerApellido;
    private String segundoApellido;
    private int identificacion;
    private String pais;
    private String avatar;
    private String clave;
    private boolean adminRoll;

    public Administrador() {
    }

    public Administrador(String nombre, String primerApellido, String segundoApellido, int identificacion, String pais, String avatar, String clave,boolean adminRoll) {
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.identificacion = identificacion;
        this.pais = pais;
        this.avatar = avatar;
        this.clave = clave;
        this.adminRoll = adminRoll;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public boolean getAdminRoll() {
        return adminRoll;
    }

    public void setAdminRoll(boolean adminRoll) {
        this.adminRoll = adminRoll;
    }

    public String toString() {
        return "Administrador{" +
                "nombre='" + nombre + '\'' +
                ", primerApellido='" + primerApellido + '\'' +
                ", segundoApellido='" + segundoApellido + '\'' +
                ", identificación=" + identificacion +
                ", pais='" + pais + '\'' +
                ", avatar='" + avatar + '\'' +
                ", clave='" + clave + '\'' +
                '}';
    }
}
