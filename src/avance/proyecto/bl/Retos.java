package avance.proyecto.bl;

public class Retos {

    private String nombreReto;
    private String codigoReto;
    private String descripcionReto;
    // private        fotoReto;
    private double kmReto;
   // private String medallaReto;
    private double costoReto;
    private String puntoInicioReto;
    private String puntoFinalReto;
    private double kmtotalesReto;


    public Retos() {
    }

    public Retos(String nombreReto, String codigoReto, String descripcionReto, double kmReto, double costoReto, String puntoInicioReto, String puntoFinalReto, double kmtotalesReto) {
        this.nombreReto = nombreReto;
        this.codigoReto = codigoReto;
        this.descripcionReto = descripcionReto;
        this.kmReto = kmReto;
        this.costoReto = costoReto;
        this.puntoInicioReto = puntoInicioReto;
        this.puntoFinalReto = puntoFinalReto;
        this.kmtotalesReto = kmtotalesReto;
    }

    public String getNombreReto() {
        return nombreReto;
    }

    public void setNombreReto(String nombreReto) {
        this.nombreReto = nombreReto;
    }

    public String getCodigoReto() {
        return codigoReto;
    }

    public void setCodigoReto(String codigoReto) {


        this.codigoReto = codigoReto;
    }

    public String getDescripcionReto() {
        return descripcionReto;
    }

    public void setDescripcionReto(String descripcionReto) {
        this.descripcionReto = descripcionReto;
    }

    public double getKmReto() {
        return kmReto;
    }

    public void setKmReto(double kmReto) {
        this.kmReto = kmReto;
    }

    public double getCostoReto() {
        return costoReto;
    }

    public void setCostoReto(double costoReto) {
        this.costoReto = costoReto;
    }

    public String getPuntoInicioReto() {
        return puntoInicioReto;
    }

    public void setPuntoInicioReto(String puntoInicioReto) {
        this.puntoInicioReto = puntoInicioReto;
    }

    public String getPuntoFinalReto() {
        return puntoFinalReto;
    }

    public void setPuntoFinalReto(String puntoFinalReto) {
        this.puntoFinalReto = puntoFinalReto;
    }

    public double getKmtotalesReto() {
        return kmtotalesReto;
    }

    public void setKmtotalesReto(double kmtotalesReto) {
        this.kmtotalesReto = kmtotalesReto;
    }

    @Override
    public String toString() {
        return "Retos{" +
                "nombreReto='" + nombreReto + '\'' +
                ", codigoReto='" + codigoReto + '\'' +
                ", descripcionReto='" + descripcionReto + '\'' +
                ", kmReto=" + kmReto +
                ", costoReto=" + costoReto +
                ", puntoInicioReto='" + puntoInicioReto + '\'' +
                ", puntoFinalReto='" + puntoFinalReto + '\'' +
                ", kmtotalesReto=" + kmtotalesReto +
                '}';
    }
}
