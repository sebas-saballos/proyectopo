package avance.proyecto.bl;

import java.time.LocalDate;

import java.util.Calendar;

public class Usuario {

    private String nombre;
    private String avatar;
    private String primerApellido;
    private String segundoApellido;
    private String identificacion;
    private String pais;
    private String nombreUsuario; // email
    private String clave;
    private LocalDate fechaNacimiento;
    private int edad;
    private char genero;
    private String direccion;
    private boolean adminRoll;


    public Usuario() {
    }


    public Usuario(int yearNacimiento,int mesNacimiento,int diaNacimiento){
        this.edad=calcularEdad(diaNacimiento,mesNacimiento,yearNacimiento);

    }

    public Usuario(String nombre, String avatar, String primerApellido, String segundoApellido, String identificacion,
                   String pais, String nombreUsuario, String clave, LocalDate fechaNacimiento,int edad,char genero,
                   String direccion,boolean adminRoll) {
        this.nombre = nombre;
        this.avatar = avatar;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.identificacion = identificacion;
        this.pais = pais;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
        this.fechaNacimiento = fechaNacimiento;
        this.edad=edad;
        this.genero = genero;
        this.direccion = direccion;
        this.adminRoll =adminRoll;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }


    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public boolean getAdminRoll() {
        return adminRoll;
    }

    public void setAdminRoll(boolean adminRoll) {
        this.adminRoll = adminRoll;
    }

    public int calcularEdad(int diaNacimiento, int mesNacimiento, int annioNacimiento){
        Calendar inicio=Calendar.getInstance();
        inicio.set(annioNacimiento,mesNacimiento-1,diaNacimiento);
        Calendar actual=Calendar.getInstance();

        int edad = actual.get(Calendar.YEAR)-inicio.get(Calendar.YEAR);
        if(inicio.get(Calendar.DAY_OF_YEAR)>actual.get(Calendar.DAY_OF_YEAR)){
            edad--;
        }
        return edad;

    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", avatar='" + avatar + '\'' +
                ", primerApellido='" + primerApellido + '\'' +
                ", segundoApellido='" + segundoApellido + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", pais='" + pais + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", clave='" + clave + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", genero=" + genero +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}


