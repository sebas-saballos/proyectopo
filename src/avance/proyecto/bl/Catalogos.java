package avance.proyecto.bl;

public class Catalogos {

    private String NomCatalogo;
    private String tipoActividad;
    private String paisCatalog;
    private int codAutoGenerate;


    public Catalogos() {
    }

    public Catalogos(String nomCatalogo, String tipoActividad, String paisCatalog, int codAutoGenerate) {
        NomCatalogo = nomCatalogo;
        this.tipoActividad = tipoActividad;
        this.paisCatalog = paisCatalog;
        this.codAutoGenerate = codAutoGenerate;
    }

    public String getNomCatalogo() {
        return NomCatalogo;
    }

    public void setNomCatalogo(String nomCatalogo) {
        NomCatalogo = nomCatalogo;
    }

    public String getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }

    public String getPaisCatalog() {
        return paisCatalog;
    }

    public void setPaisCatalog(String paisCatalog) {
        this.paisCatalog = paisCatalog;
    }

    public int getCodAutoGenerate() {
        return codAutoGenerate;
    }

    public void setCodAutoGenerate(int codAutoGenerate) {
        this.codAutoGenerate = codAutoGenerate;
    }

    @Override
    public String toString() {
        return "Catalogos: n/" +
                "NomCatalogo='" + NomCatalogo + "n/" +
                " tipoActividad='" + tipoActividad + "n/" +
                " paisCatalog='" + paisCatalog + "n/" +
                " codAutoGenerate=" + codAutoGenerate ;
    }
}

